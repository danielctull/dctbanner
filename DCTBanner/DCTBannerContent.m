//
//  DCTBannerContent.m
//  DCTBanner
//
//  Created by Daniel Tull on 24.09.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBannerContent.h"

@interface DCTBannerContent ()
@property (nonatomic) NSLayoutConstraint *widthConstraint;
@property (nonatomic) NSLayoutConstraint *topConstraint;
@end


@implementation DCTBannerContent


- (id)initWithNavigationBar:(UINavigationBar *)navigationBar {
	self = [self init];
	if (!self) return nil;
	_navigationBar = navigationBar;
	return self;
}

- (void)layoutSubviews {

	self.topConstraint = [NSLayoutConstraint constraintWithItem:self.navigationBar
													  attribute:NSLayoutAttributeBottom
													  relatedBy:NSLayoutRelationEqual
														 toItem:self
													  attribute:NSLayoutAttributeTop
													 multiplier:0.0f
													   constant:0.0f];

	self.widthConstraint = [NSLayoutConstraint constraintWithItem:self.navigationBar
														attribute:NSLayoutAttributeBottom
														relatedBy:NSLayoutRelationEqual
														   toItem:self
														attribute:NSLayoutAttributeTop
													   multiplier:0.0f
														 constant:0.0f];

	[[self.navigationBar superview] addConstraint:self.topConstraint];

	[super layoutSubviews];
	NSLog(@"%@:%@", self, NSStringFromSelector(_cmd));
}

- (void)willMoveToSuperview:(UIView *)superview {
	[super willMoveToSuperview:superview];

}

@end
