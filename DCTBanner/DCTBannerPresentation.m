//
//  DCTBannerPresentation.m
//  DCTBanner
//
//  Created by Daniel Tull on 26.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBannerPresentation.h"
#import "DCTShowInViewBannerOperation.h"
#import "DCTShowFromNavigationBarBannerOperation.h"

@interface DCTBannerPresentation ()
@property (nonatomic) NSOperationQueue *queue;
@end

@implementation DCTBannerPresentation

+ (instancetype)sharedBannerPresentation {
	static DCTBannerPresentation *sharedBannerPresentation;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedBannerPresentation = [self new];
	});
	return sharedBannerPresentation;
}

- (id)init {
	self = [super init];
	if (!self) return nil;
	_queue = [NSOperationQueue new];
	_queue.maxConcurrentOperationCount = 1;
	return self;
}

- (void)showBanner:(DCTBanner *)banner inView:(UIView *)view {
	DCTShowInViewBannerOperation *operation = [DCTShowInViewBannerOperation new];
	operation.banner = banner;
	operation.view = view;
	[self.queue addOperation:operation];
}

- (void)showBanner:(DCTBanner *)banner fromNavigationBar:(UINavigationBar *)navigationBar {
	DCTShowFromNavigationBarBannerOperation *operation = [DCTShowFromNavigationBarBannerOperation new];
	operation.banner = banner;
	operation.navigationBar = navigationBar;
	[self.queue addOperation:operation];
}

@end
