//
//  DCTShowFromNavigationBarBannerOperation.m
//  DCTBanner
//
//  Created by Daniel Tull on 28.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTShowFromNavigationBarBannerOperation.h"

@implementation DCTShowFromNavigationBarBannerOperation

- (UIView *)view {
	return [self.navigationBar superview];
}

- (UIView *)behindView {
	return self.navigationBar;
}

@end
