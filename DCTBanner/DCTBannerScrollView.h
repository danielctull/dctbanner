//
//  DCTBannerScrollView.h
//  DCTBanner
//
//  Created by Daniel Tull on 24.09.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCTBanner.h"

@interface DCTBannerScrollView : UIScrollView

- (id)initWithBanner:(DCTBanner *)banner behindView:(UIView *)behindView;

@property (nonatomic) DCTBanner *banner;
@property (nonatomic, weak) UIView *behindView;

@end
