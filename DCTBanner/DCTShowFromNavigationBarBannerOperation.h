//
//  DCTShowFromNavigationBarBannerOperation.h
//  DCTBanner
//
//  Created by Daniel Tull on 28.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBannerOperation.h"

@interface DCTShowFromNavigationBarBannerOperation : DCTBannerOperation <DCTBannerOperationSubclass>
@property (nonatomic, weak) UINavigationBar *navigationBar;
@end
