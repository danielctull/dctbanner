//
//  DCTBanner+Private.h
//  DCTBanner
//
//  Created by Daniel Tull on 24.09.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBanner.h"

@interface DCTBanner (Private)
@property (nonatomic, readonly) UIView *backgroundView;
@end
