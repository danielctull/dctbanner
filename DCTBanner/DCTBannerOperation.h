//
//  DCTBannerOperation.h
//  DCTBanner
//
//  Created by Daniel Tull on 28.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCTBanner.h"

@protocol DCTBannerOperationSubclass <NSObject>
- (UIView *)view; // View to add to
- (UIView *)behindView; // View to add behind
@end

@interface DCTBannerOperation : NSOperation

@property (nonatomic) DCTBanner *banner;

- (UIViewController *)viewControllerForView:(UIView *)view;

@end
