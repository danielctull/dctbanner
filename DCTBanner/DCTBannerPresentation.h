//
//  DCTBannerPresentation.h
//  DCTBanner
//
//  Created by Daniel Tull on 26.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCTBanner.h"

@interface DCTBannerPresentation : NSObject

+ (instancetype)sharedBannerPresentation;

- (void)showBanner:(DCTBanner *)banner inView:(UIView *)view;
- (void)showBanner:(DCTBanner *)banner fromNavigationBar:(UINavigationBar *)navigationBar;

@end
