//
//  DCTBanner.m
//  DCTBanner
//
//  Created by Daniel Tull on 25.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBanner+Private.h"
#import "DCTBannerPresentation.h"

@interface DCTBanner ()
@property (nonatomic) UINavigationBar *navigationBar;
@property (nonatomic) IBOutlet UIView *view;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@end

@implementation DCTBanner

- (id)initWithStyle:(DCTBannerStyle)style title:(NSString *)title message:(NSString *)message {

	self = [self init];
	if (!self) return nil;

	[[[self class] bundle] loadNibNamed:@"DCTBanner" owner:self options:nil];
	CGRect frame = self.view.bounds;
	self.frame = frame;

	_style = style;
	_title = [title copy];
	_message = [message copy];

	UIColor *textColor = [self textColorForStyle:style];
	self.titleLabel.text = title;
	self.titleLabel.textColor = textColor;
	self.messageLabel.text = message;
	self.messageLabel.textColor = textColor;
	[self addSubview:self.view];

	self.backgroundColor = [UIColor clearColor];
	self.clipsToBounds = YES;

	return self;
}

- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize {
	return [self.view systemLayoutSizeFittingSize:targetSize];
}

- (void)showInView:(UIView *)view {
	[[DCTBannerPresentation sharedBannerPresentation] showBanner:self inView:view];
}

- (void)showFromNavigationBar:(UINavigationBar *)navigationBar {
	[[DCTBannerPresentation sharedBannerPresentation] showBanner:self fromNavigationBar:navigationBar];
}

- (UIColor *)backgroundColorForStyle:(DCTBannerStyle)style {

	switch (style) {

		case DCTBannerStyleError:
			return [UIColor colorWithRed:1.0f green:0.131f blue:0.131f alpha:1.0f];

		case DCTBannerStyleInformation:
			return [UIColor colorWithRed:0.0f green:0.498f blue:1.0f alpha:1.0f];

		case DCTBannerStyleWarning:
			return [UIColor yellowColor];

		case DCTBannerStyleSuccess:
			return [UIColor colorWithRed:0.553f green:0.714f blue:0.0f alpha:1.0f];
	}
}

- (UIColor *)textColorForStyle:(DCTBannerStyle)style {

	switch (style) {

		case DCTBannerStyleError:
			return [UIColor whiteColor];

		case DCTBannerStyleInformation:
			return [UIColor whiteColor];

		case DCTBannerStyleWarning:
			return [UIColor blackColor];

		case DCTBannerStyleSuccess:
			return [UIColor blackColor];
	}
}

+ (NSBundle *)bundle {
	static NSBundle *bundle = nil;
	static dispatch_once_t bundleToken;
	dispatch_once(&bundleToken, ^{
		NSDirectoryEnumerator *enumerator = [[NSFileManager new] enumeratorAtURL:[[NSBundle mainBundle] bundleURL]
													  includingPropertiesForKeys:nil
																		 options:NSDirectoryEnumerationSkipsHiddenFiles
																	errorHandler:NULL];

		NSString *bundleName = [NSString stringWithFormat:@"%@.bundle", NSStringFromClass([self class])];
		for (NSURL *URL in enumerator)
			if ([[URL lastPathComponent] isEqualToString:bundleName])
				bundle = [NSBundle bundleWithURL:URL];
	});
	return bundle;
}

@end

@implementation DCTBanner (Private)

- (UIView *)backgroundView {
	UINavigationBar *navigationBar = [UINavigationBar new];
	navigationBar.barTintColor = [self backgroundColorForStyle:self.style];
	return navigationBar;
}

@end

