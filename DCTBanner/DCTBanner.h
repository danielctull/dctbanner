//
//  DCTBanner.h
//  DCTBanner
//
//  Created by Daniel Tull on 25.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DCTBannerStyle) {
    DCTBannerStyleError, // red
    DCTBannerStyleWarning, // yellow
	DCTBannerStyleInformation, // blue
	DCTBannerStyleSuccess // green
};

@interface DCTBanner : UIView

- (id)initWithStyle:(DCTBannerStyle)style title:(NSString *)title message:(NSString *)message;

@property (nonatomic, readonly) DCTBannerStyle style;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *message;

- (void)showInView:(UIView *)view;
- (void)showFromNavigationBar:(UINavigationBar *)navigationBar;

@end
