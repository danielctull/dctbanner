//
//  DCTBannerScrollView.m
//  DCTBanner
//
//  Created by Daniel Tull on 24.09.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBannerScrollView.h"
#import "DCTBanner+Private.h"


@interface DCTBannerScrollView ()
@property (nonatomic) NSLayoutConstraint *widthConstraint;
@property (nonatomic) UIView *contentView;
@property (nonatomic) UIView *backgroundView;
@end

@implementation DCTBannerScrollView

- (id)initWithBanner:(DCTBanner *)banner behindView:(UIView *)behindView {
	self = [self init];
	if (!self) return nil;
	_banner = banner;
	_behindView = behindView;

	_backgroundView = banner.backgroundView;
	[self addSubview:_backgroundView];
	[self addSubview:_banner];

	self.pagingEnabled = YES;
	self.showsVerticalScrollIndicator = NO;
	self.clipsToBounds = NO;
	self.backgroundColor = [UIColor clearColor];
	self.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);

	return self;
}

- (void)sizeToFit {
	[super sizeToFit];

	CGFloat width = self.superview.bounds.size.width;
	CGRect behindViewFrame = self.behindView.frame;
	CGSize bannerSize = [self.banner systemLayoutSizeFittingSize:CGSizeMake(width, 1000.0f)];
	bannerSize.width = width;
	CGFloat y = behindViewFrame.origin.y + behindViewFrame.size.height;
	CGFloat height = y + bannerSize.height;

	CGRect bounds = CGRectMake(0.0f, 0.0f, width, height);
	self.bounds = bounds;
	self.contentSize = CGSizeMake(bounds.size.width, bounds.size.height * 2);

	self.backgroundView.frame = CGRectMake(0.0f, -4.0f * height, width, 5.0f * height);
	self.banner.frame = CGRectMake(0.0f, y, width, bannerSize.height);

	NSLog(@"--------");
	NSLog(@"%@", NSStringFromCGRect(self.frame));
	NSLog(@"%@", NSStringFromCGRect(self.banner.frame));
	NSLog(@"%@", NSStringFromCGRect(self.backgroundView.frame));
}

@end
