	//
//  DCTBannerOperation.m
//  DCTBanner
//
//  Created by Daniel Tull on 28.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "DCTBannerOperation.h"
#import "DCTBannerScrollView.h"

@interface DCTBannerOperation () <UIScrollViewDelegate>
@property (nonatomic) BOOL executing;
@property (nonatomic) BOOL finished;
@property (nonatomic) BOOL cancelled;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) CGRect startFrame;
@property (nonatomic) CGRect endFrame;
@end

@implementation DCTBannerOperation

- (BOOL)isReady {
	return YES;
}

- (BOOL)isConcurrent {
	return YES;
}

- (BOOL)isExecuting {
	return self.executing;
}
- (void)setExecuting:(BOOL)executing {
	[self willChangeValueForKey:@"isExecuting"];
    _executing = executing;
    [self didChangeValueForKey:@"isExecuting"];
}

- (BOOL)isCancelled {
	return self.cancelled;
}
- (void)setCancelled:(BOOL)cancelled {
	[self willChangeValueForKey:@"_isCancelled"];
    _cancelled = cancelled;
    [self didChangeValueForKey:@"_isCancelled"];
}

- (BOOL)isFinished {
	return self.finished;
}
- (void)setFinished:(BOOL)finished {
	[self willChangeValueForKey:@"isFinished"];
    _finished = finished;
    [self didChangeValueForKey:@"isFinished"];
}

- (void)start {

	if (![self conformsToProtocol:@protocol(DCTBannerOperationSubclass)]) {
		[self cancel];
		return;
	}

	dispatch_async(dispatch_get_main_queue(), ^{

		DCTBannerOperation<DCTBannerOperationSubclass> *bannerOperation = (DCTBannerOperation<DCTBannerOperationSubclass> *)self;

		UIView *behindView = [bannerOperation behindView];
		self.scrollView = [[DCTBannerScrollView alloc] initWithBanner:self.banner
														   behindView:behindView];

		self.scrollView.delegate = self;
		UIView *view = [bannerOperation view];

		if (behindView)
			[view insertSubview:self.scrollView belowSubview:behindView];
		else
			[view addSubview:self.scrollView];
		
		[self.scrollView sizeToFit];


		UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
		[self.scrollView addGestureRecognizer:gesture];


		CGRect startFrame = self.scrollView.frame;
		startFrame.origin.y = -startFrame.size.height;
		startFrame.origin.x = 0.0f;

		CGRect endFrame = self.scrollView.frame;
		endFrame.origin.y = 0.0f;
		endFrame.origin.x = 0.0f;

		self.scrollView.frame = startFrame;
		[UIView animateWithDuration:0.5f animations:^{
			self.scrollView.frame = endFrame;
		} completion:^(BOOL finished) {

			double delayInSeconds = [self durationForStyle:self.banner.style];
			dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
			dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

				if (!self.scrollView.superview) return;

				[self animateViewAwayAndFinish];
			});
		}];
	});
}

- (void)cancel {
	[self.scrollView removeFromSuperview];
	self.cancelled = YES;
	self.executing = NO;
}

- (void)finish {
	[self.scrollView removeFromSuperview];
	self.finished = YES;
	self.executing = NO;
}

- (void)animateViewAwayAndFinish {
	CGRect frame = self.scrollView.frame;
	frame.origin.y = -frame.size.height;
	[UIView animateWithDuration:0.5f animations:^{
		self.scrollView.frame = frame;
	} completion:^(BOOL finished) {
		[self finish];
	}];
}

- (UIViewController *)viewControllerForView:(UIView *)view {

	if (!view) return nil;

	id responder = view.nextResponder;

	if ([responder isKindOfClass:[UIViewController class]])
		return responder;

	if ([responder isKindOfClass:[UIView class]])
		return [self viewControllerForView:responder];

	return [self viewControllerForView:view.superview];
}

- (NSTimeInterval)durationForStyle:(DCTBannerStyle)style {

	switch (style) {

		case DCTBannerStyleError:
			return 5.0f;

		case DCTBannerStyleInformation:
			return 4.0f;

		case DCTBannerStyleWarning:
			return 3.0f;

		case DCTBannerStyleSuccess:
			return 2.0f;
	}
}

#pragma mark - UITapGestureRecognizer

- (void)tapGestureRecognized:(UITapGestureRecognizer *)gesture {
	[self animateViewAwayAndFinish];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	if (scrollView.contentOffset.y > 0) [self finish];
}

@end
