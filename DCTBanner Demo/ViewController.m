//
//  ViewController.m
//  DCTBanner
//
//  Created by Daniel Tull on 25.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "ViewController.h"
#import <DCTBanner/DCTBanner.h>

@interface ViewController ()
@end

@implementation ViewController

- (IBAction)showError:(id)sender {
	DCTBanner *banner = [[DCTBanner alloc] initWithStyle:DCTBannerStyleError
												   title:@"Error"
												 message:@"There has been an error of some sort."];
	[banner showFromNavigationBar:self.navigationController.navigationBar];
}

- (IBAction)showWarning:(id)sender {
	DCTBanner *banner = [[DCTBanner alloc] initWithStyle:DCTBannerStyleWarning
												   title:@"Warning"
												 message:@"This is a warning to you. Never try that again!"];
	[banner showFromNavigationBar:self.navigationController.navigationBar];
}

- (IBAction)showInformation:(id)sender {
	DCTBanner *banner = [[DCTBanner alloc] initWithStyle:DCTBannerStyleInformation
												   title:@"Information"
												 message:@"You tapped on the button to show this information."];
	[banner showFromNavigationBar:self.navigationController.navigationBar];
}

- (IBAction)showSuccess:(id)sender {
	DCTBanner *banner = [[DCTBanner alloc] initWithStyle:DCTBannerStyleSuccess
												   title:@"Success"
												 message:@"Congratulations! You're a big success."];
	[banner showFromNavigationBar:self.navigationController.navigationBar];
}

@end
